%global srcname aiohttp-jinja2

Name:          python-%{srcname}
Summary:       Jinja2 template renderer for aiohttp.web
License:       ASL 2.0
URL:           https://aiohttp-jinja2.readthedocs.io/en/stable/

Version:       1.2.0
Release:       1%{?dist}
Source0:       %{pypi_source}

BuildArch:     noarch


%global _description %{expand:
Jinja2 template renderer for aiohttp.web.}

%description %_description


%package -n python3-%{srcname}
Summary:       %{summary}

BuildRequires: python3-devel
BuildRequires: %{py3_dist aiohttp} >= 3.2.0
BuildRequires: %{py3_dist jinja2} >= 2.10.1
BuildRequires: %{py3_dist pytest}
BuildRequires: %{py3_dist pytest-aiohttp}
BuildRequires: %{py3_dist setuptools}

%{?python_provide:%python_provide python3-%{srcname}}


%description -n python3-%{srcname} %_description


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py3_build


%install
%py3_install


%check
# Tests don't find the module without this
touch tests/__init__.py

# Work around deprecation warnings in Jinja2:
# https://github.com/pallets/jinja/commit/31bf9b7e71c3fee3b7866ffdc0f70f4525a490d9#diff-8f76b794bd641afb7bd92eba51f332f3
py.test-3 -W ignore::DeprecationWarning


%files -n python3-%{srcname}
%doc README.rst
%license LICENSE
%{python3_sitelib}/aiohttp_jinja2
%{python3_sitelib}/aiohttp_jinja2-%{version}-py*.egg-info


%changelog
* Fri Nov 01 2019 Mathieu Bridon <bochecha@daitauha.fr> - 1.2.0-1
- Initial package for Fedora.
